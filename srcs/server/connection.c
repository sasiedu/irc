/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   connection.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 17:12:47 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:50:26 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	*get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET)
		return (&(((struct sockaddr_in*)sa)->sin_addr));
	return (&(((struct sockaddr_in6*)sa)->sin6_addr));
}

int		new_connection(t_server_info *s, socklen_t addrlen)
{
	struct sockaddr_storage		remoteaddr;
	char						remote_ip[INET6_ADDRSTRLEN];
	int							optval;

	addrlen = sizeof(remoteaddr);
	s->newfd = accept(s->listener, (struct sockaddr *)&remoteaddr,
			&addrlen);
	if (s->newfd == -1)
		return (printf("Error: failed to accept() new connection\n"));
	printf("Server: new connection from %s on socket %d\n",
			inet_ntop(remoteaddr.ss_family, get_in_addr((struct sockaddr*)
			&remoteaddr), remote_ip, INET6_ADDRSTRLEN), s->newfd);
	optval = 1;
	setsockopt(s->newfd, SOL_TCP, TCP_NODELAY, &optval, 4);
	add_socket(s->socks, create_new_socket(s->newfd, 1, READ),
			ft_itoa_base(s->newfd, 10));
	add_active_user(&g_active_users, new_active_user(s->newfd, NULL));
	return (0);
}

int		read_data(t_server_info *s, int socket, char *line)
{
	int			ret;
	char		*data;
	t_parser	cmd;

	(void)s;
	cmd.user = NULL;
	cmd.params = NULL;
	cmd.socket = socket;
	data = ft_strdup(line);
	while (ft_strlen(line) == 100)
	{
		bzero(line, sizeof(char) * 100);
		if (recv(socket, line, 100, 0) <= 0)
			break ;
		data = ft_strcat(data, line);
	}
	ret = process_line(&cmd, line);
	my_lexer(&cmd, ret);
	return (0);
}

void	remove_active(int socket, int i)
{
	t_a_user	*active;
	t_lst		*chans;
	t_chan		*chan;
	char		buf[1024];

	active = get_active_user(g_active_users, socket, NULL, NULL);
	if (active == NULL || active->user == NULL)
		return ;
	chans = active->user->chans;
	while (chans != NULL)
	{
		snprintf(buf, 1024, "[%s] %s went offline\n", (char *)chans->data,
				active->user->nick);
		chan = get_channel(g_channels, (char *)chans->data);
		while (++i < 100 && chan != NULL)
		{
			if (chan->active[i] != NULL && chan->active[i] != active)
				quick_send(chan->active[i]->socket, buf);
			else if (chan->active[i] != NULL && chan->active[i] == active)
				chan->active[i] = NULL;
		}
		chans = chans->next;
	}
	delete_active_user(&g_active_users, NULL, socket, NULL, NULL);
}

int		read_connection(t_server_info *s, int socket)
{
	int			status;
	char		buf[101];

	bzero(buf, sizeof(char) * 101);
	status = recv(socket, buf, 100, 0);
	if (status == 0)
	{
		remove_socket(s->socks, ft_itoa_base(socket, 10));
		close(socket);
		remove_active(socket, -1);
		printf("Lost connection to socket %d\n", socket);
		return (1);
	}
	read_data(s, socket, buf);
	return (0);
}
