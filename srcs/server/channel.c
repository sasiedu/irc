/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   channel.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 14:50:54 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:25:32 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

t_chan		*new_channel(char *name, t_a_user *user)
{
	t_chan		*new;

	if (name == NULL)
		return (NULL);
	if ((new = (t_chan *)malloc(sizeof(t_chan))) == NULL)
		return (NULL);
	memset(new->admins, 0, sizeof(t_user *) * 100);
	memset(new->users, 0, sizeof(t_user *) * 100);
	memset(new->active, 0, sizeof(t_a_user *) * 100);
	new->name = ft_strdup(name);
	new->pass = NULL;
	new->admins[0] = user->user;
	new->users[0] = user->user;
	new->active[0] = user;
	new->next = NULL;
	return (new);
}

void		add_channel(t_chan **chans, t_chan *chan)
{
	if (chans == NULL)
		return ;
	if (*chans == NULL)
	{
		*chans = chan;
		return ;
	}
	if ((*chans)->next == NULL)
	{
		(*chans)->next = chan;
		return ;
	}
	add_channel(&(*chans)->next, chan);
}

t_chan		*get_channel(t_chan *chan, char *name)
{
	if (chan == NULL || name == NULL)
		return (NULL);
	if (!ft_strcmp(chan->name, name))
		return (chan);
	return (get_channel(chan->next, name));
}

void		delete_channel(t_chan **chans, t_chan *chan, char *name)
{
	t_chan	*tmp;

	if (chans == NULL || *chans == NULL || (name == NULL &&
				chan == NULL))
		return ;
	if (*chans == chan || !ft_strcmp((*chans)->name, name))
	{
		tmp = *chans;
		*chans = (*chans)->next;
		ft_strdel(&tmp->name);
		ft_strdel(&tmp->pass);
		free(tmp);
	}
	delete_channel(&(*chans)->next, chan, name);
}
