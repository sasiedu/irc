/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 01:56:12 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:36:12 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

t_lst	*new_lst(void *data, size_t size)
{
	t_lst	*new;

	if (data == NULL)
		return (NULL);
	if ((new = (t_lst *)malloc(sizeof(t_lst))) == NULL)
		return (NULL);
	new->data = (void *)ft_strdup((char *)data);
	new->size = size;
	new->next = NULL;
	return (new);
}

void	add_lst(t_lst **lsts, t_lst *lst)
{
	if (lsts == NULL || lst == NULL)
		return ;
	if (*lsts == NULL)
	{
		*lsts = lst;
		return ;
	}
	if ((*lsts)->next == NULL)
	{
		(*lsts)->next = lst;
		return ;
	}
	add_lst(&(*lsts)->next, lst);
}

t_lst	*get_lst(t_lst *lst, void *data, size_t size)
{
	if (lst == NULL || data == NULL || size < 1)
		return (NULL);
	if (!ft_memcmp(lst->data, data, size))
		return (lst);
	return (get_lst(lst->next, data, size));
}

void	delete_lst(t_lst **lsts, void *data, size_t size, t_lst *lst)
{
	t_lst	*tmp;

	if (lsts == NULL || *lsts == NULL)
		return ;
	if ((data == NULL || size < 1) && lst == NULL)
		return ;
	if (*lsts == lst || !ft_memcmp((*lsts)->data, data, size))
	{
		tmp = *lsts;
		*lsts = (*lsts)->next;
		ft_memdel(&tmp->data);
		free(tmp);
		return ;
	}
	delete_lst(&(*lsts)->next, data, size, lst);
}
