/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 08:03:03 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:35:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	leave_channel(t_a_user *user, t_chan *chan)
{
	int		i;
	char	buf[1024];

	if (chan == NULL || user == NULL)
		return ;
	remove_chan_user(chan, user->user, NULL, NULL);
	remove_chan_user(chan, NULL, NULL, user);
	i = -1;
	snprintf(buf, 1024, "[%s] %s left the channel\n", chan->name,
			user->user->nick);
	while (++i < 100)
	{
		if (chan->active[i] != NULL)
			quick_send(chan->active[i]->socket, buf);
	}
}

void	leave_command(t_parser *cmd, t_a_user *user)
{
	t_lst	*lst;

	if (cmd->num_params == 0)
	{
		lst = user->user->chans;
		while (lst != NULL)
		{
			leave_channel(user, get_channel(g_channels, (char *)lst->data));
			delete_lst(&user->user->chans, (char *)lst->data,
				lst->size, NULL);
			lst = user->user->chans;
		}
		return ;
	}
	if (get_lst(user->user->chans, (char *)cmd->params,
				ft_strlen(cmd->params)) == NULL)
		return ;
	leave_channel(user, get_channel(g_channels, cmd->params));
	delete_lst(&user->user->chans, (char *)cmd->params,
			ft_strlen(cmd->params), NULL);
}

void	who_command(t_parser *cmd, t_a_user *user)
{
	t_chan	*chan;
	char	buf[1024];
	int		i;

	(void)user;
	if (cmd->num_params != 1)
		return (quick_send(cmd->socket, "Invalid parameters\n"));
	if ((chan = get_channel(g_channels, cmd->params)) == NULL)
	{
		snprintf(buf, 1024, "No channel : %s", cmd->params);
		return (quick_send(cmd->socket, buf));
	}
	i = -1;
	while (++i < 100)
	{
		if (chan->users[i] != NULL)
		{
			if (get_active_user(g_active_users, -1, chan->users[i], NULL)
					!= NULL)
				snprintf(buf, 1024, ":%s [online]\n", chan->users[i]->nick);
			else
				snprintf(buf, 1024, ":%s [offline]\n", chan->users[i]->nick);
			quick_send(cmd->socket, buf);
		}
	}
}

void	msg_channel(t_parser *cmd, t_a_user *user)
{
	char		buf[1024];
	char		**split;
	t_chan		*chan;
	int			i;

	if ((split = ft_strsplit(cmd->line, ' ')) == NULL)
		return (quick_send(cmd->socket, "Server error\n"));
	bzero(buf, 1024);
	if ((chan = get_channel(g_channels, split[1])) == NULL)
	{
		snprintf(buf, 1024, "No channel name %s\n", split[1]);
		return (quick_send(cmd->socket, buf));
	}
	i = -1;
	snprintf(buf, 1024, "[%s : %s] %s\n", split[1], user->user->nick,
			get_skip_line(cmd->line, 2, ' '));
	while (++i < 100)
	{
		if (chan->active[i] != NULL)
			quick_send(chan->active[i]->socket, buf);
	}
	ft_del_array(split);
}

void	msg_command(t_parser *cmd, t_a_user *user)
{
	t_a_user	*tmp;
	char		**split;
	char		buf[1024];

	(void)user;
	if (cmd->num_params < 2)
		return (quick_send(cmd->socket, "Invalid parameters\n"));
	if ((split = ft_strsplit(cmd->line, ' ')) == NULL)
		return (quick_send(cmd->socket, "Server error\n"));
	if (cmd->params[0] == '#')
		return (msg_channel(cmd, user));
	tmp = get_active_user(g_active_users, -1, NULL, split[1]);
	ft_del_array(split);
	if (tmp == NULL)
		return (quick_send(cmd->socket, "No user or user offline\n"));
	ft_memset(buf, 0, sizeof(char) * 1024);
	ft_strcpy(buf, "*");
	ft_strcat(buf, user->user->nick);
	ft_strcat(buf, "* : ");
	ft_strcat(buf, get_skip_line(cmd->line, 2, ' '));
	quick_send(tmp->socket, buf);
}
