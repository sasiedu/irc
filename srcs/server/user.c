/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/05 06:12:00 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:44:48 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

t_user		*new_user(char *nick, char *name, char *pass)
{
	t_user	*new_user;

	if ((new_user = (t_user *)malloc(sizeof(t_user))) == NULL)
		return (NULL);
	new_user->nick = ft_strdup(nick);
	new_user->name = (name == NULL) ? NULL : ft_strdup(name);
	new_user->pass = (pass == NULL) ? NULL : ft_strdup(pass);
	new_user->chans = NULL;
	new_user->next = NULL;
	return (new_user);
}

void		add_user(t_user **users, t_user *user)
{
	if (users == NULL)
		return ;
	if (*users == NULL)
	{
		*users = user;
		return ;
	}
	if ((*users)->next == NULL)
	{
		(*users)->next = user;
		return ;
	}
	add_user(&(*users)->next, user);
}

t_user		*get_user(t_user *user, char *nick, char *name)
{
	if (user == NULL || (nick == NULL && name == NULL))
		return (NULL);
	if (nick != NULL && name != NULL && !ft_strcmp(user->nick,
		nick) && !ft_strcmp(user->name, name))
		return (user);
	if (nick != NULL && !ft_strcmp(user->nick, nick))
		return (user);
	if (name != NULL && !ft_strcmp(user->name, name))
		return (user);
	return (get_user(user->next, nick, name));
}

void		remove_user(t_user **users, t_user *user, char *name, char *nick)
{
	if (users == NULL || *users == NULL)
		return ;
	if (user == NULL && nick == NULL && name == NULL)
		return ;
	if (*users == user || (name != NULL && nick != NULL &&
		!strcmp((*users)->name, name) && !strcmp((*users)->nick, nick))
		|| (nick != NULL && !ft_strcmp((*users)->nick, nick)) ||
		(name != NULL && !ft_strcmp((*users)->name, name)))
	{
		*users = (*users)->next;
		return ;
	}
	remove_user(&(*users)->next, user, name, nick);
}

void		delete_user(t_user **users, t_user *user, char *name, char *nick)
{
	if (users == NULL || *users == NULL)
		return ;
	if (user == NULL && nick == NULL && name == NULL)
		return ;
	if (*users == user || (name != NULL && nick != NULL &&
		!strcmp((*users)->name, name) && !strcmp((*users)->nick, nick))
		|| (nick != NULL && !ft_strcmp((*users)->nick, nick)) ||
		(name != NULL && !ft_strcmp((*users)->name, name)))
	{
		*users = (*users)->next;
		ft_strdel(&user->nick);
		ft_strdel(&user->name);
		ft_strdel(&user->pass);
		free(user);
		return ;
	}
	remove_user(&(*users)->next, user, name, nick);
}
