/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   active_user.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/05 06:45:45 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:41:28 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

t_a_user	*new_active_user(int socket, t_user *user)
{
	t_a_user	*new;

	if (socket < 0)
		return (NULL);
	if ((new = (t_a_user *)malloc(sizeof(t_a_user))) == NULL)
		return (NULL);
	new->user = user;
	new->socket = socket;
	new->auth = 1;
	new->next = NULL;
	return (new);
}

void		add_active_user(t_a_user **users, t_a_user *user)
{
	if (users == NULL)
		return ;
	if (*users == NULL)
	{
		*users = user;
		return ;
	}
	if ((*users)->next == NULL)
	{
		(*users)->next = user;
		return ;
	}
	add_active_user(&(*users)->next, user);
}

t_a_user	*get_active_user(t_a_user *active,  int socket, t_user *user,
		char *nick)
{
	if (active == NULL)
		return (NULL);
	if (socket == -1 && user == NULL && nick == NULL)
		return (NULL);
	if (socket != -1 && socket == active->socket)
		return (active);
	if (user != NULL && user == active->user)
		return (active);
	if (active->user != NULL && !ft_strcmp(nick, active->user->nick))
		return (active);
	return (get_active_user(active->next, socket, user, nick));
}

void		remove_active_user(t_a_user **active, t_a_user *a_user,
		int socket, char *nick, t_user *user)
{
	if (active == NULL || *active == NULL)
		return ;
	if (user == NULL && socket == -1 && nick == NULL && a_user == NULL)
		return ;
	if (*active == a_user || (*active)->user == user || (*active)->socket ==
			socket || ((*active)->user != NULL &&
			!ft_strcmp((*active)->user->nick, nick)))
	{
		*active = (*active)->next;
		return ;
	}
	remove_active_user(&(*active)->next, a_user, socket, nick, user);
}

void		delete_active_user(t_a_user **active, t_a_user *a_user,
		int socket, char *nick, t_user *user)
{
	t_a_user	*tmp;

	if (active == NULL || *active == NULL)
		return ;
	if (user == NULL && socket == -1 && nick == NULL && a_user == NULL)
		return ;
	if (*active == a_user || (*active)->user == user || (*active)->socket ==
			socket || ((*active)->user != NULL &&
			!ft_strcmp((*active)->user->nick, nick)))
	{
		tmp = *active;
		*active = (*active)->next;
		free(tmp);
		return ;
	}
	remove_active_user(&(*active)->next, a_user, socket, nick, user);
}
