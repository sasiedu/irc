/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   channel2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 02:51:31 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:27:56 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			add_chan_user(t_chan *chan, t_user *user, t_user *admin,
		t_a_user *active)
{
	int		i;

	if (chan == NULL || (user == NULL && admin == NULL && active == NULL))
		return (0);
	i = -1;
	while (++i < 100)
	{
		if (user != NULL && chan->users[i] == NULL)
		{
			chan->users[i] = user;
			return (1);
		}
		if (admin != NULL && chan->admins[i] == NULL)
		{
			chan->admins[i] = admin;
			return (1);
		}
		if (active != NULL && chan->active[i] == NULL)
		{
			chan->active[i] = active;
			return (1);
		}
	}
	return (0);
}

int			remove_chan_user(t_chan *chan, t_user *user, t_user *admin,
		t_a_user *active)
{
	int		i;

	if (chan == NULL || (user == NULL && admin == NULL && active == NULL))
		return (0);
	i = -1;
	while (++i < 100)
	{
		if (user != NULL && chan->users[i] == user)
		{
			chan->users[i] = NULL;
			return (1);
		}
		if (admin != NULL && chan->admins[i] == admin)
		{
			chan->admins[i] = NULL;
			return (1);
		}
		if (active != NULL && chan->active[i] == active)
		{
			chan->active[i] = NULL;
			return (1);
		}
	}
	return (0);
}

t_user		*get_chan_admin(t_chan *chan, t_user *user, char *nick)
{
	int		i;

	if (chan == NULL || (user == NULL && nick == NULL))
		return (NULL);
	i = 0;
	while (i < 100)
	{
		if (chan->admins[i] != NULL)
		{
			if (user != NULL && chan->admins[i] == user)
				return (chan->admins[i]);
			if (nick != NULL && !ft_strcmp(chan->admins[i]->name, nick))
				return (chan->admins[i]);
		}
		i++;
	}
	return (NULL);
}

t_user		*get_chan_users(t_chan *chan, t_user *user, char *nick)
{
	int		i;

	if (chan == NULL || (user == NULL && nick == NULL))
		return (NULL);
	i = 0;
	while (i < 100)
	{
		if (chan->users[i] != NULL)
		{
			if (user != NULL && chan->users[i] == user)
				return (chan->users[i]);
			if (nick != NULL && !ft_strcmp(chan->users[i]->name, nick))
				return (chan->users[i]);
		}
		i++;
	}
	return (NULL);
}

t_a_user	*get_chan_active(t_chan *chan, t_a_user *user, char *nick)
{
	int		i;

	if (chan == NULL || (user == NULL && nick == NULL))
		return (NULL);
	i = 0;
	while (i < 100)
	{
		if (chan->active[i] != NULL)
		{
			if (user != NULL && chan->active[i] == user)
				return (chan->active[i]);
			if (nick != NULL && !ft_strcmp(chan->active[i]->user->name,
						nick))
				return (chan->active[i]);
		}
		i++;
	}
	return (NULL);
}
