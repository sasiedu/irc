/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/27 08:21:13 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:40:43 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

size_t		get_tab_size(char **tab)
{
	size_t		i;

	if (tab == NULL)
		return (0);
	i = 0;
	while (tab[i] != NULL)
	{
		i++;
	}
	return (i);
}

void		get_cmd_user(t_parser *cmd, size_t *i, char **tab)
{
	if (tab[0][0] == ':')
	{
		cmd->user = ft_strdup(tab[0]);
		*i += 1;
	}
}

int			get_cmd(t_parser *cmd, size_t *i, char **tab)
{
	static char	*cmds[] = {"nick", "join", "leave", "who", "msg", "connect",
			"kick", "oper", "pass", "ping", "privmsg", "quit", "die", "kill"};
	int			j;

	if (tab[*i] == NULL)
		return (-1);
	j = 0;
	while (j < 15)
	{
		if (ft_strcmp(cmds[j], tab[*i]) == 0)
		{
			cmd->cmd = ft_strdup(tab[*i]);
			*i += 1;
			return (0);
		}
		j++;
	}
	return (1);
}

void		get_cmd_params(t_parser *cmd, size_t *i, char **tab)
{
	char	*tmp;

	cmd->num_params = 0;
	while (tab[*i] != NULL)
	{
		cmd->num_params += 1;
		tab[*i][ft_strlen(tab[*i])] = '\0';
		if (cmd->params == NULL)
			cmd->params = strdup(tab[*i]);
		else
		{
			tmp = ft_strjoin(cmd->params, ",");
			free(cmd->params);
			cmd->params = ft_strjoin(tmp, tab[*i]);
			free(tmp);
		}
		*i += 1;
	}
}

int			process_line(t_parser *cmd, char *line)
{
	char		**tab;
	size_t		i;

	if (line[ft_strlen(line) - 1] == '\n')
		line[ft_strlen(line) - 1] = '\0';
	if (line == NULL || ft_strlen(line) < 3)
		return (-1);
	if ((tab = ft_strsplit(line, ' ')) == NULL)
		return (-1);
	if (get_tab_size(tab) > 17)
		return (1);
	cmd->line = ft_strdup(line);
	i = 0;
	get_cmd_user(cmd, &i, tab);
	if (get_cmd(cmd, &i, tab) == 0)
	{
		get_cmd_params(cmd, &i, tab);
		ft_del_array(tab);
		return (0);
	}
	ft_del_array(tab);
	return (2);
}
