/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other_server.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/23 10:55:40 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:42:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		is_port_inuse(int port)
{
	int					sock;
	struct sockaddr_in	server;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	bzero((char *)&server, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = port;
	if (bind(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		printf("Port is not available. Already in use\n");
		return (-1);
	}
	close(sock);
	return (0);
}

void	check_for_other_servers(t_server_info *s, int fd, int sock, char *port)
{
	char				*line;
	struct sockaddr_in	server;
	t_sock				*new;

	if ((fd = open("server.list", O_RDONLY)) == -1)
		return ;
	while (get_next_line(fd, &line) > 0)
	{
		ft_putendl(line);
		if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
			continue ;
		if (ft_strcmp(line, port) == 0)
			continue ;
		server.sin_addr.s_addr = inet_addr("127.0.0.1");
		server.sin_family = AF_INET;
		server.sin_port = htons(ft_atoi(line));
		if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
			continue ;
		new = create_new_socket(sock, 1, READ);
		add_socket(s->socks, new, ft_itoa(sock));
	}
	close(fd);
}

void	add_to_server_list(char *port)
{
	int		fd;
	char	*line;

	if ((fd = open("server.list", O_RDWR | O_APPEND)) == -1)
		return ;
	while (get_next_line(fd, &line) > 0)
	{
		if (ft_strcmp(port, line) == 0)
		{
			free(line);
			close(fd);
			return ;
		}
	}
	ft_putendl_fd(port, fd);
	close(fd);
}
