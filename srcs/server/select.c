/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 17:09:01 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:50:27 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		check_read_fds(t_server_info *s)
{
	int		sock;

	sock = 0;
	(void)s;
	while (sock <= s->maxfd)
	{
		if (FD_ISSET(sock, &s->read_fds))
		{
			if (sock == s->listener)
				new_connection(s, 0);
			else
				read_connection(s, sock);
		}
		sock++;
	}
	return (0);
}
