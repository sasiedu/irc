/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 13:53:57 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:43:57 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		create_listener(t_server_info *s, struct addrinfo *p, int optval)
{
	socklen_t	tmp;

	while (p != NULL)
	{
		s->listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (s->listener >= 0)
		{
			if (setsockopt(s->listener, SOL_SOCKET, SO_REUSEADDR, &optval,
						sizeof(int)) != 0)
				return (printf("Error: setsockopt() failed\n"));
			if (bind(s->listener, p->ai_addr, p->ai_addrlen) >= 0)
				break ;
			close(s->listener);
		}
		p = p->ai_next;
	}
	if (p == NULL)
		return (printf("failed to bind server\n"));
	tmp = sizeof(int);
	getsockopt(s->listener, SOL_SOCKET, SO_REUSEADDR, &optval, &tmp);
	printf("reuse val : %d\n", optval);
	return (0);
}

int		init_server(t_server_info *s, int port, char *str_port)
{
	struct addrinfo		hints;
	struct addrinfo		*res;
	struct protoent		*protocol;
	int					ret;

	if (port > 65535 || port < 2000)
		return (printf("Error: Invalid port number\n"));
	protocol = getprotobyname("tcp");
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = protocol->p_proto;
	if (getaddrinfo(NULL, str_port, &hints, &res) != 0)
		return (printf("Error: Failed to getaddrinfo\n"));
	ret = create_listener(s, res, 0);
	freeaddrinfo(res);
	return (ret);
}

int		setup_server(t_server_info *s)
{
	if (listen(s->listener, 20) == -1)
		return (printf("Error: listen() on server listener\n"));
	s->socks = create_new_socket(s->listener, 1, READ);
	return (0);
}

void	start_server_loop(t_server_info *s, int port)
{
	struct timeval	tv;
	int				ret;

	tv.tv_sec = 2;
	tv.tv_usec = 0;
	check_for_other_servers(s, 0, 0, ft_itoa(port));
	add_to_server_list(ft_itoa(port));
	printf("Server Started on port : %d\n", port);
	printf("Waiting for connections\n");
	while (1)
	{
		FD_ZERO(&s->write_fds);
		FD_ZERO(&s->read_fds);
		s->maxfd = 0;
		add_to_fdset(s->socks, &s->read_fds, &s->write_fds, &s->maxfd);
		if ((ret = select(s->maxfd + 1, &s->read_fds, &s->write_fds, NULL,
						&tv)) == -1)
		{
			perror("select()");
			exit(2);
		}
		if (ret > 0)
			check_read_fds(s);
	}
}

int		main(int ac, char **av)
{
	t_server_info	s_info;

	if (ac != 2)
		return (printf("Error: usage : ./server <port_number>\n"));
	if (is_port_inuse(ft_atoi(av[1])) == -1)
		return (1);
	if (init_server(&s_info, atoi(av[1]), av[1]) != 0)
		exit(2);
	if (setup_server(&s_info) != 0)
		exit(2);
	g_users = NULL;
	g_active_users = NULL;
	g_channels = NULL;
	start_server_loop(&s_info, atoi(av[1]));
	return (0);
}
