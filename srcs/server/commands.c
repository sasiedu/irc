/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/05 07:58:06 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:35:27 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	nick_command(t_parser *cmd, t_a_user *user)
{
	t_user		*tmp;
	t_lst		*chans;
	t_chan		*chan;

	if (cmd->num_params != 1 || ft_strlen(cmd->params) > 9)
		return (quick_send(cmd->socket, "Invalid parameters\n"));
	if (get_active_user(g_active_users, -1, NULL, cmd->params) != NULL)
		return (quick_send(cmd->socket, "Nick already active\n"));
	if ((tmp = get_user(g_users, cmd->params, NULL)) == NULL)
		add_user(&g_users, (tmp = new_user(cmd->params, NULL, NULL)));
	user->user = tmp;
	user->auth = (tmp->pass == NULL) ? 1 : 0;
	if (!user->auth)
		return (quick_send(cmd->socket, "Password required\n"));
	quick_send(cmd->socket, "Success: logged in\n");
	chans = tmp->chans;
	while (chans != NULL)
	{
		chan = get_channel(g_channels, (char *)chans->data);
		add_chan_user(chan, NULL, NULL, user);
		chans = chans->next;
	}
}

void	pass_command(t_parser *cmd, t_a_user *user)
{
	if (cmd->num_params != 1)
		return (quick_send(cmd->socket, "Invalid parameters\n"));
	if (!user->auth)
	{
		if (ft_strcmp(user->user->pass, cmd->params) != 0)
			return (quick_send(cmd->socket, "Password incorrect\n"));
		user->auth = 1;
		return (quick_send(cmd->socket, "Password correct\n"));
	}
	if (user->user->pass != NULL)
		free(user->user->pass);
	user->user->pass = ft_strdup(cmd->params);
	user->auth = 1;
	quick_send(cmd->socket, "Success: password set\n");
}

void	join_channel2(t_chan *tmp, char *buf, t_a_user *user, char *name)
{
	int		i;

	i = -1;
	if (!add_chan_user(tmp, user->user, NULL, NULL))
	{
		snprintf(buf, 1024, "Channel [%s] is full\n", name);
		return (quick_send(user->socket, buf));
	}
	add_chan_user(tmp, NULL, NULL, user);
	add_lst(&user->user->chans, new_lst(name, ft_strlen(name)));
	snprintf(buf, 1024, "[%s] %s joined channel\n", name,
			user->user->nick);
	while (++i < 100)
	{
		if (tmp->active[i] != NULL)
			quick_send(tmp->active[i]->socket, buf);
	}
}

void	join_channel(char *name, char *pass, t_a_user *user)
{
	t_chan	*tmp;
	char	buf[1024];

	memset(buf, 0, sizeof(char) * 1024);
	if ((tmp = get_channel(g_channels, name)) == NULL)
	{
		tmp = new_channel(name, user);
		tmp->pass = ft_strdup(pass);
		add_channel(&g_channels, tmp);
		add_lst(&user->user->chans, new_lst(name, ft_strlen(name)));
		snprintf(buf, 1024, "Channel created : %s\n", name);
		return (quick_send(user->socket, buf));
	}
	if (get_chan_users(tmp, user->user, NULL) != NULL)
	{
		snprintf(buf, 1024, "Already a member of channel : %s\n", name);
		return (quick_send(user->socket, buf));
	}
	if (tmp->pass != NULL && ft_strcmp(tmp->pass, pass) != 0)
	{
		snprintf(buf, 1024, "Invalid password for channel : %s\n", name);
		return (quick_send(user->socket, buf));
	}
	join_channel2(tmp, buf, user, name);
}

void	join_command(t_parser *cmd, t_a_user *user)
{
	char	**split;
	char	**chans;
	char	**pass;
	int		count;

	if (cmd->num_params != 1 && cmd->num_params != 2)
		return (quick_send(cmd->socket, "Invalid parameters\n"));
	if ((split = ft_strsplit(cmd->line, ' ')) == NULL)
		return (quick_send(cmd->socket, "Server error\n"));
	chans = ft_strsplit(split[1], ',');
	if (cmd->num_params == 2)
		pass = ft_strsplit(split[2], ',');
	count = (int)ft_array_size(chans);
	while (--count >= 0)
	{
		chans[count][ft_strlen(chans[count])] = '\0';
		if (cmd->num_params == 2 && count < (int)ft_array_size(pass))
			join_channel(chans[count], pass[count], user);
		else
			join_channel(chans[count], NULL, user);
	}
	ft_del_array(split);
	ft_del_array(chans);
	if (cmd->num_params == 2)
		ft_del_array(pass);
}
