/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/23 07:33:38 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:42:33 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

t_sock		*create_new_socket(int socket, uint8_t open, int type)
{
	t_sock	*new;
	int		i;

	if ((new = (t_sock *)malloc(sizeof(t_sock))) == NULL)
		return (NULL);
	new->socket = socket;
	new->open = open;
	new->type = type;
	i = 0;
	while (i < 10)
	{
		new->socks[i] = NULL;
		i++;
	}
	return (new);
}

void		add_socket(t_sock *sock, t_sock *new, char *index)
{
	int		i;

	if (!ft_isdigit(index[0]))
	{
		sock->socket = new->socket;
		sock->type = new->type;
		sock->open = new->open;
		free(new);
		return ;
	}
	i = (int)index[0] - 48;
	if (sock->socks[i] == NULL)
	{
		sock->socks[i] = (t_sock *)malloc(sizeof(t_sock));
		bzero(sock->socks[i]->socks, sizeof(t_sock *) * 10);
	}
	add_socket(sock->socks[i], new, &index[1]);
}

void		remove_socket(t_sock *sock, char *index)
{
	int		i;

	if (!ft_isdigit(index[0]))
	{
		sock->socket = -1;
		sock->type = -1;
		sock->open = 0;
		return ;
	}
	i = (int)index[0] - 48;
	if (sock->socks[i] != NULL)
		remove_socket(sock->socks[i], &index[1]);
}

t_sock		*get_socket(t_sock *sock, char *index)
{
	int		i;

	if (!ft_isdigit(index[0]))
		return (sock);
	i = (int)index[0] - 48;
	if (sock->socks[i] != NULL)
		return (get_socket(sock->socks[i], &index[1]));
	return (NULL);
}

void		add_to_fdset(t_sock *socks, fd_set *read, fd_set *write, int *maxfd)
{
	size_t	i;

	i = 0;
	if (socks->socket != -1 && socks->open == 1 && socks->type == READ)
		FD_SET(socks->socket, read);
	if (socks->socket != -1 && socks->open == 1 && socks->type == WRITE)
		FD_SET(socks->socket, write);
	if (socks->socket != -1 && socks->open == 1)
		*maxfd = (socks->socket > *maxfd) ? socks->socket : *maxfd;
	while (i < 10)
	{
		if (socks->socks[i] != NULL)
			add_to_fdset(socks->socks[i], read, write, maxfd);
		i++;
	}
}
