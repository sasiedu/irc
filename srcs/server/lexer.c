/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/25 13:20:56 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:50:23 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		my_lexer(t_parser *cmd, int ret)
{
	(void)cmd;
	if (ret != 0)
	{
		if (ret == -1)
			send(cmd->socket, "Server Error\n", 13, 0);
		else if (ret == 1)
			send(cmd->socket, "Too many parameters\n", 20, 0);
		else if (ret == 2)
			send(cmd->socket, "Invalid command\n", 16, 0);
		return (1);
	}
	process_command(cmd, get_active_user(g_active_users,
		cmd->socket, NULL, NULL));
	return (0);
}

void	quick_send(int socket, char *msg)
{
	send(socket, msg, ft_strlen(msg), 0);
}

char	*get_skip_line(char *line, int count, char c)
{
	char	*tmp;

	tmp = line;
	while (count > 0)
	{
		if ((tmp = strchr(tmp, c)) == NULL)
			return (NULL);
		++tmp;
		count--;
	}
	return (tmp);
}

void	process_command(t_parser *cmd, t_a_user *user)
{
	int		i;

	i = 0;
	while (g_cmd_funcs[i].cmd != NULL)
	{
		if (!ft_strcmp(cmd->cmd, g_cmd_funcs[i].cmd))
		{
			if (ft_strcmp(cmd->cmd, "nick") != 0 && user->user == NULL)
				return (quick_send(cmd->socket, "Error: set nick first\n"));
			g_cmd_funcs[i].func(cmd, user);
			return ;
		}
		i++;
	}
}
