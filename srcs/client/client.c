/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 08:53:35 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:41:37 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int		init_prompt(t_prompt *p)
{
	memset(p->buf, 0, sizeof(char) * 1024);
	memset(p->tmp, 0, sizeof(char) * 1024);
	memset(p->login, 0, sizeof(char) * 9);
	p->index = 0;
	p->history = NULL;
	p->his_index = 0;
	p->exit = 0;
	p->len = 0;
	ft_strcpy(p->login, "login");
	initscr();
	timeout(1);
	keypad(stdscr, TRUE);
	noecho();
	return (0);
}

int		destroy_prompt(t_prompt *p)
{
	(void)p;
	endwin();
	return (0);
}

void	get_key_input(t_prompt *p, t_client *c)
{
	int		ch;

	ch = getch();
	if (ch == KEY_UP || ch == KEY_DOWN || ch == KEY_RIGHT || ch == KEY_LEFT)
		return (arrow_keys(p, ch));
	if (ch >= 32 && ch < 127)
		return (add_char(p, ch));
	if (ch == KEY_BACKSPACE || ch == 127)
		return (remove_char(p));
	if (ch == 10)
		process_line(p, c);
}

int		main(int ac, char **av)
{
	t_prompt	prompt;
	t_client	client;

	client.sock = -1;
	init_prompt(&prompt);
	if (ac > 1)
		setup_client(&client, av[1], (ac > 2) ? ft_atoi(av[2]) : 3001);
	print_prompt(&prompt);
	while (!prompt.exit)
	{
		get_key_input(&prompt, &client);
		select_connection(&client, &prompt);
		refresh();
	}
	endwin();
	return (0);
}
