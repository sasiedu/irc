/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arrow.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 11:03:44 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 11:09:07 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void	left_key(t_prompt *p)
{
	if (p->index == 0)
		return ;
	p->index--;
	wmove(stdscr, p->pos_y, p->pos_x + p->index);
}

void	right_key(t_prompt *p)
{
	if (p->index == (int)p->len)
		return ;
	p->index++;
	wmove(stdscr, p->pos_y, p->pos_x + p->index);
}

void	arrow_keys(t_prompt *p, int ch)
{
	if (ch == KEY_LEFT)
		left_key(p);
	else if (ch == KEY_RIGHT)
		right_key(p);
}
