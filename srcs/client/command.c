/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 12:57:35 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:41:34 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void	process_connect(t_prompt *p, t_client *c)
{
	char	**split;
	size_t	size;

	if (c->sock != -1)
	{
		close(c->sock);
		c->sock = -1;
	}
	if ((split = ft_strsplit(p->buf, ' ')) == NULL)
		return ;
	size = ft_array_size(split);
	if (size == 1)
		printw("Invalid parameters");
	else if (size == 2)
		setup_client(c, split[1], 3001);
	else if (size > 2)
		setup_client(c, split[1], ft_atoi(split[2]));
	if (c->sock != -1)
		printw("Connected to %s", split[1]);
	ft_del_array(split);
	wmove(stdscr, p->pos_y + 2, 0);
}

void	process_command(t_prompt *p, t_client *c)
{
	char	*login;

	if (!ft_strcmp(p->buf, "exit"))
		p->exit = 1;
	else if (ft_strncmp(p->buf, "connect", 7) != 0)
	{
		send(c->sock, p->buf, p->len, 0);
		if (!ft_strncmp("nick", p->buf, 4) && p->len < 14)
		{
			if ((login = strchr(p->buf, ' ')) != NULL)
				ft_strcpy(p->login, login + 1);
		}
	}
	else
		process_connect(p, c);
}

void	process_line(t_prompt *p, t_client *c)
{
	wmove(stdscr, p->pos_y + 1, 0);
	if (c->sock > 0 || !ft_strncmp(p->buf, "connect", 7))
		process_command(p, c);
	else if (p->len > 1)
	{
		printw("No connection");
		wmove(stdscr, p->pos_y + 2, 0);
	}
	ft_memset(p->buf, 0, 1024);
	p->len = 0;
	p->index = 0;
	print_prompt(p);
}
