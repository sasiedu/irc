/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 09:42:46 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:42:23 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void	print_prompt(t_prompt *p)
{
	attron(A_BOLD);
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	attron(COLOR_PAIR(1));
	printw("%s$ ", p->login);
	attroff(COLOR_PAIR(1));
	attroff(A_BOLD);
	getyx(stdscr, p->pos_y, p->pos_x);
}

void	add_char(t_prompt *p, char c)
{
	size_t	i;
	char	tmp;

	if (p->len == 1023)
		return ;
	i = p->index;
	while (i < p->len)
	{
		tmp = p->buf[i];
		p->buf[i++] = c;
		c = tmp;
	}
	p->buf[i++] = c;
	p->buf[i] -= '\0';
	p->index++;
	p->len++;
	wmove(stdscr, p->pos_y, p->pos_x);
	clrtoeol();
	printw("%s", p->buf);
	wmove(stdscr, p->pos_y, p->pos_x + p->index);
}

void	remove_char(t_prompt *p)
{
	size_t	i;

	if (p->len == 0 || p->index == 0)
		return ;
	i = p->index;
	while (i <= p->len)
	{
		p->buf[i - 1] = p->buf[i];
		i++;
	}
	p->index--;
	p->len--;
	wmove(stdscr, p->pos_y, p->pos_x);
	clrtoeol();
	printw("%s", p->buf);
	wmove(stdscr, p->pos_y, p->pos_x + p->index);
}
