/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   connect.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 08:58:43 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:43:30 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void	setup_client(t_client *client, char *addr, int port)
{
	struct sockaddr_in	server;

	if ((client->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printw("Error: failed to create a socket\n");
		client->sock = -1;
		return ;
	}
	server.sin_addr.s_addr = inet_addr(addr);
	server.sin_family = AF_UNSPEC;
	server.sin_port = htons(port);
	if (connect(client->sock, (struct sockaddr *)&server, sizeof(server))
			< 0)
	{
		printw("Error: failed to connect to %s\n", addr);
		close(client->sock);
		client->sock = -1;
		return ;
	}
}

void	process_recv(t_client *client)
{
	char	buf[1024];

	bzero(buf, sizeof(char) * 1024);
	if (recv(client->sock, buf, 1023, 0) == 0)
	{
		printw("Lost connection\n");
		close(client->sock);
		client->sock = -1;
		return ;
	}
	printw("%s", buf);
}

void	select_connection(t_client *client, t_prompt *p)
{
	struct timeval	tv;
	int				ret;

	if (client->sock == -1)
		return ;
	wmove(stdscr, p->pos_y, 0);
	clrtoeol();
	tv.tv_sec = 0;
	tv.tv_usec = 5000;
	FD_ZERO(&client->read_fd);
	FD_SET(client->sock, &client->read_fd);
	if ((ret = select(client->sock + 1, &client->read_fd, NULL, NULL, &tv))
			== -1)
		printw("select error\n");
	if (ret > 0 && FD_ISSET(client->sock, &client->read_fd))
		process_recv(client);
	print_prompt(p);
	printw("%s", p->buf);
	wmove(stdscr, p->pos_y, p->pos_x + p->index);
}
