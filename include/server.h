/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 13:54:21 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 08:50:35 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "shared.h"

t_user				*g_users;
t_a_user			*g_active_users;
t_chan				*g_channels;

int					new_connection(t_server_info *s, socklen_t addrlen);
int					check_read_fds(t_server_info *s);
int					read_connection(t_server_info *s, int socket);

t_sock				*create_new_socket(int socket, uint8_t open, int type);
void				add_socket(t_sock *sock, t_sock *new, char *index);
void				remove_socket(t_sock *sock, char *index);
t_sock				*get_socket(t_sock *sock, char *index);
void				add_to_fdset(t_sock *socks, fd_set *read,
						fd_set *write, int *maxfd);

void				check_for_other_servers(t_server_info *s, int fd,
		int sock, char *port);
void				add_to_server_list(char *port);
int					is_port_inuse(int port);
int					process_line(t_parser *cmd, char *line);
int					my_lexer(t_parser *cmd, int ret);
void				quick_send(int socket, char *msg);
char				*get_skip_line(char *line, int count, char c);
void				process_command(t_parser *cmd, t_a_user *user);

/*
*** user functions
*/
t_user				*new_user(char *nick, char *name, char *pass);
void				add_user(t_user **users, t_user *user);
t_user				*get_user(t_user *user, char *nick, char *name);
void				remove_user(t_user **users, t_user *user, char *name,
						char *nick);
void				delete_user(t_user **users, t_user *user, char *name,
						char *nick);

/*
*** active user functions
*/
t_a_user			*new_active_user(int socket, t_user *user);
void				add_active_user(t_a_user **users, t_a_user *user);
t_a_user			*get_active_user(t_a_user *active, int socket,
						t_user *user, char *nick);
void				remove_active_user(t_a_user **active, t_a_user *a_user,
		int socket, char *nick, t_user *user);
void				delete_active_user(t_a_user **active, t_a_user *a_user,
		int socket, char *nick, t_user *user);

/*
*** lst functions
*/
t_lst				*new_lst(void *data, size_t size);
void				add_lst(t_lst **lsts, t_lst *lst);
t_lst				*get_lst(t_lst *lst, void *data, size_t size);
void				delete_lst(t_lst **lsts, void *data, size_t size,
		t_lst *lst);

/*
***  channel functions
*/
t_chan				*new_channel(char *name, t_a_user *user);
void				add_channel(t_chan **chans, t_chan *chan);
t_chan				*get_channel(t_chan *chan, char *name);
void				delete_channel(t_chan **chans, t_chan *chan, char *name);
t_a_user			*get_chan_active(t_chan *chan, t_a_user *user, char *nick);
t_user				*get_chan_admin(t_chan *chan, t_user *user, char *nick);
t_user				*get_chan_users(t_chan *chan, t_user *user, char *nick);
int					add_chan_user(t_chan *chan, t_user *user, t_user *admin,
		t_a_user *active);
int					remove_chan_user(t_chan *chan, t_user *user, t_user *admin,
		t_a_user *active);

/*
*** command functions
*/
void				nick_command(t_parser *cmd, t_a_user *user);
void				pass_command(t_parser *cmd, t_a_user *user);
void				msg_command(t_parser *cmd, t_a_user *user);
void				join_command(t_parser *cmd, t_a_user *user);
void				leave_command(t_parser *cmd, t_a_user *user);
void				who_command(t_parser *cmd, t_a_user *user);

static	t_cmd_funcs	g_cmd_funcs[] =
{
	{"nick", nick_command},
	{"pass", pass_command},
	{"msg", msg_command},
	{"join", join_command},
	{"leave", leave_command},
	{"who", who_command},
	{NULL, NULL}
};
#endif
