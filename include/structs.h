/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 11:52:33 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:41:35 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H
# define STRUCTS_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>
# include <netinet/tcp.h>
# include "libft.h"

# define SOL_TCP	6
# define READ		1
# define WRITE		2

typedef	struct		s_sock
{
	int				socket;
	int				type;
	uint8_t			open;
	struct s_sock	*socks[10];
}					t_sock;

typedef	struct		s_server_info
{
	fd_set			write_fds;
	fd_set			read_fds;
	int				maxfd;
	int				listener;
	int				newfd;
	t_sock			*socks;
}					t_server_info;

typedef	struct		s_client
{
	fd_set			read_fd;
	int				sock;
}					t_client;

typedef	struct		s_lst
{
	void			*data;
	size_t			size;
	struct s_lst	*next;
}					t_lst;

typedef	struct		s_user
{
	char			*nick;
	char			*name;
	char			*pass;
	t_lst			*chans;
	struct s_user	*next;
}					t_user;

/*
***  a_user means active user
*/

typedef	struct		s_a_user
{
	t_user			*user;
	int				socket;
	uint8_t			auth;
	struct s_a_user	*next;
}					t_a_user;

typedef	struct		s_chan
{
	t_user			*admins[100];
	t_user			*users[100];
	t_a_user		*active[100];
	char			*pass;
	char			*name;
	struct s_chan	*next;
}					t_chan;

typedef struct		s_parser
{
	char			*user;
	char			*cmd;
	char			*params;
	char			*line;
	int				socket;
	size_t			num_params;
}					t_parser;

typedef	struct		s_cmd_funcs
{
	char			*cmd;
	void			(*func)(t_parser *, t_a_user *);
}					t_cmd_funcs;

typedef	struct		s_prompt
{
	char			buf[1024];
	char			tmp[1024];
	char			login[9];
	size_t			len;
	int				index;
	int				pos_y;
	int				pos_x;
	t_lst			*history;
	int				his_index;
	int				exit;
}					t_prompt;

#endif
