/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shared.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 11:46:50 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 09:09:10 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHARED_H
# define SHARED_H

# include "structs.h"

/*
*** active user functions
*/
t_a_user			*new_active_user(int socket, t_user *user);
void				add_active_user(t_a_user **users, t_a_user *user);
t_a_user			*get_active_user(t_a_user *active, int socket,
						t_user *user, char *nick);
void				remove_active_user(t_a_user **active, t_a_user *a_user,
		int socket, char *nick, t_user *user);
void				delete_active_user(t_a_user **active, t_a_user *a_user,
		int socket, char *nick, t_user *user);

#endif
