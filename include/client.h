/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 08:53:51 by sasiedu           #+#    #+#             */
/*   Updated: 2017/08/08 13:44:14 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_H
# define CLIENT_H

# include "shared.h"
# include <ncurses.h>

void		print_prompt(t_prompt *p);
void		add_char(t_prompt *p, char c);
void		remove_char(t_prompt *p);
void		arrow_keys(t_prompt *p, int ch);
void		setup_client(t_client *client, char *addr, int port);
void		select_connection(t_client *client, t_prompt *p);
void		process_line(t_prompt *p, t_client *c);

#endif
