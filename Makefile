# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/07/20 10:48:53 by sasiedu           #+#    #+#              #
#    Updated: 2017/07/20 11:45:46 by sasiedu          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

server:
	make -C srcs/server

client:
	make -C srcs/client

all: server client

cleanserver:
	make -C srcs/server clean

cleanclient:
	make -C srcs/client clean

fcleanserver:
	make -C srcs/server fclean
	/bin/rm -rf server

fcleanclient:
	make -C srcs/client fclean
	/bin/rm -rf client

clean: cleanserver cleanclient

fclean: fcleanclient fcleanserver

reserver: fcleanserver server

reclient: fcleanclient client

re: reserver reclient
